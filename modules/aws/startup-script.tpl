#! /bin/bash

apt-get update
apt-get upgrade

apt-get install -y docker.io docker-compose
usermod -aG docker ubuntu

docker run -d -p 80:80 -p 443:443 nginx

${extra_bootstrap_cmds}

touch /.startup-done
