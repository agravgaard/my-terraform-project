variable "instance_name" {
  description = "Value of the Name tag for the EC2 instance"
  type        = string
  default     = "ExampleAppServerInstance"
}

variable "instance_type" {
  description = "EC2 instance type tier.size"
  type        = string
  default     = "t4g.nano"
}

variable "aws_region" {
  description = "AWS region"
  default     = "eu-west-1"
}

variable "availability_zone" {
  description = "availability zone to create subnet"
  default     = "eu-west-1a"
}

variable "extra_bootstrap_cmds" {
  description = "Additional commands to run during instance bootstrap"
  default     = ""
}
