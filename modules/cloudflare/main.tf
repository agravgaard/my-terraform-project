terraform {
  required_providers {
    # Configure the Cloudflare provider using the required_providers stanza required with Terraform 0.13 and beyond
    # You may optionally use version directive to prevent breaking changes occurring unannounced.
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 2.0"
    }
  }
}

provider "cloudflare" {
  email   = var.cloudflare_email
  api_key = var.cloudflare_api_key
}

# Add a record to the domain
resource "cloudflare_record" "ec2_dns_record" {
  zone_id = var.cloudflare_zone_id
  name    = var.domain
  value   = var.public_ip
  type    = "A"
  ttl     = 3600
}
