variable "domain" {
  description = "Value of the Name tag for the EC2 instance"
  type        = string
}

variable "public_ip" {
  description = "IPv4 address of the EC2 instance"
  type        = string
}

variable "cloudflare_zone_id" {
  description = "Cloudflare ZoneID for domain"
  type        = string
}

variable "cloudflare_email" {
  description = "Cloudflare account email"
  type        = string
  sensitive   = true
}

variable "cloudflare_api_key" {
  description = "Cloudflare API key (not token)"
  type        = string
  sensitive   = true
}
