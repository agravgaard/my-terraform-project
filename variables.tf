variable "domain" {
  description = "Domain to register with EC2 instance on cloudflare"
  type        = string
  default     = "ec2.cyberbunny.cyou"
}

variable "instance_type" {
  description = "EC2 instance type tier.size"
  type        = string
  default     = "t4g.nano"
}

variable "instance_name" {
  description = "Value of the Name tag for the EC2 instance"
  type        = string
  default     = "ExampleAppServerInstance"
}

variable "aws_region" {
  description = "AWS region"
  default     = "eu-west-1"
}

variable "availability_zone" {
  description = "availability zone to create subnet"
  default     = "eu-west-1a"
}

variable "extra_bootstrap_cmds" {
  description = "Additional commands to run during instance bootstrap"
  default     = ""
}

variable "cloudflare_zone_id" {
  description = "Cloudflare ZoneID for domain"
  type        = string
  default     = "f52866a9b2cda7c92fee68a186f45203"
}

variable "cloudflare_email" {
  description = "Cloudflare account email"
  type        = string
  sensitive   = true
}

variable "cloudflare_api_key" {
  description = "Cloudflare API key (not token)"
  type        = string
  sensitive   = true
}
