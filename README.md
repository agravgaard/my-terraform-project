## How to use

Set the following environment variables (or use a `.tfvars` file)
```
export TF_VAR_domain=yourdomain.com
export TF_VAR_cloudflare_email=you@somedomain.com
export TF_VAR_cloudflare_api_key=<numbers and lower-case letters>
```
Use the API **key** not token.

Run
```
terraform init
terraform apply
```
