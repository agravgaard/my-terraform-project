terraform {
  required_version = ">= 0.14.9"

  backend "http" {
  }
}

module "aws" {
  source = "./modules/aws"

  aws_region           = var.aws_region
  instance_name        = var.instance_name
  instance_type        = var.instance_type
  availability_zone    = var.availability_zone
  extra_bootstrap_cmds = var.extra_bootstrap_cmds
}

module "cloudflare" {
  source = "./modules/cloudflare"

  domain             = var.domain
  cloudflare_email   = var.cloudflare_email
  cloudflare_api_key = var.cloudflare_api_key
  cloudflare_zone_id = var.cloudflare_zone_id
  public_ip          = module.aws.instance_public_ip
}
