output "instance_id" {
  description = "ID of the EC2 instance"
  value       = module.aws.instance_id
}

output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = module.aws.instance_public_ip
}
